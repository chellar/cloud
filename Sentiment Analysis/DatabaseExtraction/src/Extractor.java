/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Class used to extract data from the tweets database, perform sentiment analysis and store the result
* in the suburb-teams database
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.lightcouch.CouchDbClient;
import org.lightcouch.Document;

public class Extractor {

	ArrayList<String> positiveWords;
	ArrayList<String> negativeWords;
	ArrayList<SuburbTeams> suburbTeams;
	
	public Extractor() {
		positiveWords = new ArrayList<String>();
		negativeWords = new ArrayList<String>();
		suburbTeams = new ArrayList<SuburbTeams>();
	}
	
	public static void main(String[] args) {
		Extractor extractor = new Extractor();
		extractor.setPositiveWords();
		extractor.setNegativeWords();
		extractor.extract();
	}
	
	// extracts the tweets, perform sentiment analysis and store results in the database
	public void extract()  {
		CouchDbClient dbClientTweets = new CouchDbClient("tweets", true, "http", "115.146.93.181", 5984, "admin", "123456");
		CouchDbClient dbClientSuburb = new CouchDbClient("suburb-team", true, "http", "115.146.93.181", 5984, "admin", "123456");
		// extracts all the documents from the suburb database
		// mapreduce is not required since all the data are relevant
		List<Document> suburbDocuments = dbClientSuburb.view("_all_docs").includeDocs(true).query(Document.class);
		for(int index = 0; index < suburbDocuments.size(); index++)
		{
			SuburbTeams suburbTeam = dbClientSuburb.find(SuburbTeams.class, suburbDocuments.get(index).getId());
			suburbTeam.reset();
			suburbTeams.add(suburbTeam);
		}
		try {
			// extract all the tweets from the database
			// no map reduce in this case as well since only relevant tweets are present
			List<Document> documents = dbClientTweets.view("_all_docs").includeDocs(true).query(Document.class);
			for(int count = 0; count < documents.size(); count++)
			{
				Tweets tweet = dbClientTweets.find(Tweets.class, documents.get(count).getId());
				String suburb = tweet.getSuburb();
				String tweetText = tweet.getTweetText();
				String[] words = tweetText.split(" ");
				Arrays.sort(words);
				int sentiment = 0;
				Iterator<String> positiveIterator = positiveWords.iterator();
				while(positiveIterator.hasNext()) {
					String positive = positiveIterator.next();
					int found = Arrays.binarySearch(words, positive);
					if(found >= 0) {
						sentiment = sentiment + 1;
					}
				}
				Iterator<String> negativeIterator = negativeWords.iterator();
				while(negativeIterator.hasNext()) {
					String negative = negativeIterator.next();
					int found = Arrays.binarySearch(words, negative);
					if(found >= 0) {
						sentiment = sentiment - 1;
					}
				}
				Iterator<SuburbTeams> iterator = suburbTeams.iterator();
				int index = 0;
				// checks for the suburb in order to store it in the right location
				while(iterator.hasNext()) {
					SuburbTeams suburbTeam = iterator.next();
					if(suburbTeam.getSuburb().equalsIgnoreCase(suburb)) {
						suburbTeam.setTweetsPresent(true);
						break;
					}
					index = index + 1;
				}
				if(index < suburbTeams.size()) {
					searchTeam(tweetText, index, sentiment);
					if(tweet.getSentiment() != 0) {
						dbClientTweets.update(tweet);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		// updates the database once the analysis is complete
		Iterator<SuburbTeams> iterator = suburbTeams.iterator();
		while(iterator.hasNext()) {
			SuburbTeams suburbTeam = iterator.next();
			dbClientSuburb.update(suburbTeam);
		}
	}
	
	// extracts and stores all the negativ keywords
	public void setNegativeWords() {
		try {
			File file = new File("negative.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while (inputKeywords.hasNextLine()) {
				String negative = inputKeywords.nextLine();
				negativeWords.add(negative);
			}
			inputKeywords.close();
		} catch (Exception ex) {
		}
	}
	
	// extracts and stores all the positive keywords
	public void setPositiveWords() {
		try {
			File file = new File("positive.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while (inputKeywords.hasNextLine()) {
				String positive = inputKeywords.nextLine();
				positiveWords.add(positive);
			}
			inputKeywords.close();
		} catch (Exception ex) {
		}
	}
	
	// searches for a specific team in the tweet and sets the sentiment of that tweet
	public void searchTeam(String tweetText, int index, int sentiment) {
		if(tweetText.contains("Adelaide Crows") || tweetText.contains("#adelaidecrows")
				|| tweetText.contains("#AFLCrowsTigers")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setAdelaideCrowsPositive(suburbTeams.get(index).getAdelaideCrowsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setAdelaideCrowsNegative(suburbTeams.get(index).getAdelaideCrowsNegative() + 1);
			} else {
				suburbTeams.get(index).setAdelaideCrowsTotal(suburbTeams.get(index).getAdelaideCrowsTotal() + 1);
			}
		} else if(tweetText.contains("Geelong Cats") || tweetText.contains("#AFLCatsPies")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setGeelongCatsPositive(suburbTeams.get(index).getGeelongCatsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setGeelongCatsNegative(suburbTeams.get(index).getGeelongCatsNegative() + 1);
			} else {
				suburbTeams.get(index).setGeelongCatsTotal(suburbTeams.get(index).getGeelongCatsTotal() + 1);
			}
		} else if(tweetText.contains("GWS Giants") || tweetText.contains("#NRLSouthsManly")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setGWSGiantsPositive(suburbTeams.get(index).getGWSGiantsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setGWSGiantsNegative(suburbTeams.get(index).getGWSGiantsNegative() + 1);
			} else {
				suburbTeams.get(index).setGWSGiantsTotal(suburbTeams.get(index).getGWSGiantsTotal() + 1);
			}
		} else if(tweetText.contains("Western Bulldogs") || tweetText.contains("#AFLGiantsDogs")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setWesternBulldogsPositive(suburbTeams.get(index).getWesternBulldogsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setWesternBulldogsNegative(suburbTeams.get(index).getWesternBulldogsNegative() + 1);
			} else {
				suburbTeams.get(index).setWesternBulldogsTotal(suburbTeams.get(index).getWesternBulldogsTotal() + 1);
			}
		} if(tweetText.contains("West Coast Eagles") || tweetText.contains("#AFLEaglesFreo")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setWestCoastEaglesPositive(suburbTeams.get(index).getWestCoastEaglesPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setWestCoastEaglesNegative(suburbTeams.get(index).getWestCoastEaglesNegative() + 1);
			} else {
				suburbTeams.get(index).setWestCoastEaglesTotal(suburbTeams.get(index).getWestCoastEaglesTotal() + 1);
			}
		} if(tweetText.contains("Gold Coast Suns") || tweetText.contains("#AFLNorthSuns")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setGoldCoastSunsPositive(suburbTeams.get(index).getGoldCoastSunsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setGoldCoastSunsNegative(suburbTeams.get(index).getGoldCoastSunsNegative() + 1);
			} else {
				suburbTeams.get(index).setGoldCoastSunsTotal(suburbTeams.get(index).getGoldCoastSunsTotal() + 1);
			}
		} if(tweetText.contains("Brisbane Lions") || tweetText.contains("#brisbanelions")
				|| tweetText.contains("#AFLLionsPower")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setBrisbaneLionsPositive(suburbTeams.get(index).getBrisbaneLionsPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setBrisbaneLionsNegative(suburbTeams.get(index).getBrisbaneLionsNegative() + 1);
			} else {
				suburbTeams.get(index).setBrisbaneLionsTotal(suburbTeams.get(index).getBrisbaneLionsTotal() + 1);
			}
		} if(tweetText.contains("Hawthorn")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setHawthornPositive(suburbTeams.get(index).getHawthornPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setHawthornNegative(suburbTeams.get(index).getHawthornNegative() + 1);
			} else {
				suburbTeams.get(index).setHawthornTotal(suburbTeams.get(index).getHawthornTotal() + 1);
			}
		} if(tweetText.contains("Sydney Swans") || tweetText.contains("#AFLBluesSwans")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setSydneySwansPositive(suburbTeams.get(index).getSydneySwansPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setSydneySwansNegative(suburbTeams.get(index).getSydneySwansNegative() + 1);
			} else {
				suburbTeams.get(index).setSydneySwansTotal(suburbTeams.get(index).getSydneySwansTotal() + 1);
			}
		} if(tweetText.contains("FremantleFC")) {
			if(sentiment > 0) {
				suburbTeams.get(index).setFremantleFCPositive(suburbTeams.get(index).getFremantleFCPositive() + 1);
			} else if (sentiment < 0) {
				suburbTeams.get(index).setFremantleFCNegative(suburbTeams.get(index).getFremantleFCNegative() + 1);
			} else {
				suburbTeams.get(index).setFremantleFCTotal(suburbTeams.get(index).getFremantleFCTotal() + 1);
			}
		}
		// since all the tweets are about AFL, the sentiment is updated in all cases
		if(sentiment > 0) {
			suburbTeams.get(index).setAFLPositive(suburbTeams.get(index).getAFLPositive() + 1);
		} else if (sentiment < 0) {
			suburbTeams.get(index).setAFLNegative(suburbTeams.get(index).getAFLNegative() + 1);
		} else {
			suburbTeams.get(index).setAFLTotal(suburbTeams.get(index).getAFLTotal() + 1);
		}
	}

}
