/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Team used to store the team based stats - Table in the UI
*/

package suburb;

public class TeamStats {
	private String suburbName;
	private int positiveTweets;
	private int negativeTweets;
	private int neutralTweets;
	
	public TeamStats(String suburbName, int positiveTweets, int negativeTweets, int neutralTweets) {
		this.suburbName = suburbName;
		this.positiveTweets = positiveTweets;
		this.negativeTweets = negativeTweets;
		this.neutralTweets = neutralTweets;
	}
	
	public String getSuburbName() {
		return suburbName;
	}
	
	public void setSuburbName(String suburbName) {
		this.suburbName = suburbName;
	}
	
	public int getPositiveTweets() {
		return positiveTweets;
	}
	
	public void setPositiveTweets(int positiveTweets) {
		this.positiveTweets = positiveTweets;
	}
	
	public int getNegativeTweets() {
		return negativeTweets;
	}
	
	public void setNegativeTweets(int negativeTweets) {
		this.negativeTweets = negativeTweets;
	}
	
	public int getNeutralTweets() {
		return neutralTweets;
	}
	
	public void setNeutralTweets(int neutralTweets) {
		this.neutralTweets = neutralTweets;
	}
	
}
