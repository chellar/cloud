/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Class used to store the analysed results
*/

package suburb;


public class Suburb extends org.lightcouch.Document {

	private String suburb;
	private Double maxLongitude;
	private Double minLongitude;
	private Double maxLatitude;
	private Double minLatitude;
	private int AdelaideCrowsPositive;
	private int AdelaideCrowsNegative;
	private int AdelaideCrowsTotal;
	private int GeelongCatsPositive;
	private int GeelongCatsNegative;
	private int GeelongCatsTotal;
	private int GWSGiantsPositive;
	private int GWSGiantsNegative;
	private int GWSGiantsTotal;
	private int WesternBulldogsPositive;
	private int WesternBulldogsNegative;
	private int WesternBulldogsTotal;
	private int WestCoastEaglesPositive;
	private int WestCoastEaglesNegative;
	private int WestCoastEaglesTotal;
	private int FremantleFCPositive;
	private int FremantleFCNegative;
	private int FremantleFCTotal;
	private int GoldCoastSunsPositive;
	private int GoldCoastSunsNegative;
	private int GoldCoastSunsTotal;
	private int BrisbaneLionsPositive;
	private int BrisbaneLionsNegative;
	private int BrisbaneLionsTotal;
	private int HawthornPositive;
	private int HawthornNegative;
	private int HawthornTotal;
	private int SydneySwansPositive;
	private int SydneySwansNegative;
	private int SydneySwansTotal;
	private int AFLPositive;
	private int AFLNegative;
	private int AFLTotal;
	private Boolean tweetsPresent;
	
	public Suburb(String suburb, Double maxLongitude,
			Double minLongitude, Double maxLatitude, Double minLatitude) {
		this.setId(suburb);
		this.suburb = suburb;
		this.maxLongitude = maxLongitude;
		this.minLongitude = minLongitude;
		this.maxLatitude = maxLatitude;
		this.minLatitude = minLatitude;
		this.tweetsPresent = false;
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public Double getMaxLongitude() {
		return maxLongitude;
	}
	
	public void setMaxLongitude(Double maxLongitude) {
		this.maxLongitude = maxLongitude;
	}
	
	public Double getMinLongitude() {
		return minLongitude;
	}
	
	public void setMinLongitude(Double minLongitude) {
		this.minLongitude = minLongitude;
	}
	
	public Double getMaxLatitude() {
		return maxLatitude;
	}
	
	public void setMaxLatitude(Double maxLatitude) {
		this.maxLatitude = maxLatitude;
	}
	
	public Double getMinLatitude() {
		return minLatitude;
	}
	
	public void setMinLatitude(Double minLatitude) {
		this.minLatitude = minLatitude;
	}
	
	public int getAdelaideCrowsPositive() {
		return AdelaideCrowsPositive;
	}
	
	public void setAdelaideCrowsPositive(int adelaideCrowsPositive) {
		AdelaideCrowsPositive = adelaideCrowsPositive;
	}
	
	public int getAdelaideCrowsNegative() {
		return AdelaideCrowsNegative;
	}
	
	public void setAdelaideCrowsNegative(int adelaideCrowsNegative) {
		AdelaideCrowsNegative = adelaideCrowsNegative;
	}
	
	public int getAdelaideCrowsTotal() {
		return AdelaideCrowsTotal;
	}
	
	public void setAdelaideCrowsTotal(int adelaideCrowsTotal) {
		AdelaideCrowsTotal = adelaideCrowsTotal;
	}
	
	public int getGeelongCatsPositive() {
		return GeelongCatsPositive;
	}
	
	public void setGeelongCatsPositive(int geelongCatsPositive) {
		GeelongCatsPositive = geelongCatsPositive;
	}
	
	public int getGeelongCatsNegative() {
		return GeelongCatsNegative;
	}
	
	public void setGeelongCatsNegative(int geelongCatsNegative) {
		GeelongCatsNegative = geelongCatsNegative;
	}
	
	public int getGeelongCatsTotal() {
		return GeelongCatsTotal;
	}
	
	public void setGeelongCatsTotal(int geelongCatsTotal) {
		GeelongCatsTotal = geelongCatsTotal;
	}
	
	public int getGWSGiantsPositive() {
		return GWSGiantsPositive;
	}
	
	public void setGWSGiantsPositive(int gWSGiantsPositive) {
		GWSGiantsPositive = gWSGiantsPositive;
	}
	
	public int getGWSGiantsNegative() {
		return GWSGiantsNegative;
	}
	
	public void setGWSGiantsNegative(int gWSGiantsNegative) {
		GWSGiantsNegative = gWSGiantsNegative;
	}
	
	public int getGWSGiantsTotal() {
		return GWSGiantsTotal;
	}
	
	public void setGWSGiantsTotal(int gWSGiantsTotal) {
		GWSGiantsTotal = gWSGiantsTotal;
	}
	
	public int getWesternBulldogsPositive() {
		return WesternBulldogsPositive;
	}
	
	public void setWesternBulldogsPositive(int westernBulldogsPositive) {
		WesternBulldogsPositive = westernBulldogsPositive;
	}
	
	public int getWesternBulldogsNegative() {
		return WesternBulldogsNegative;
	}
	
	public void setWesternBulldogsNegative(int westernBulldogsNegative) {
		WesternBulldogsNegative = westernBulldogsNegative;
	}
	
	public int getWesternBulldogsTotal() {
		return WesternBulldogsTotal;
	}
	
	public void setWesternBulldogsTotal(int westernBulldogsTotal) {
		WesternBulldogsTotal = westernBulldogsTotal;
	}
	
	public int getWestCoastEaglesPositive() {
		return WestCoastEaglesPositive;
	}
	
	public void setWestCoastEaglesPositive(int westCoastEaglesPositive) {
		WestCoastEaglesPositive = westCoastEaglesPositive;
	}
	
	public int getWestCoastEaglesNegative() {
		return WestCoastEaglesNegative;
	}
	
	public void setWestCoastEaglesNegative(int westCoastEaglesNegative) {
		WestCoastEaglesNegative = westCoastEaglesNegative;
	}
	
	public int getWestCoastEaglesTotal() {
		return WestCoastEaglesTotal;
	}
	
	public void setWestCoastEaglesTotal(int westCoastEaglesTotal) {
		WestCoastEaglesTotal = westCoastEaglesTotal;
	}
	
	public int getFremantleFCPositive() {
		return FremantleFCPositive;
	}
	
	public void setFremantleFCPositive(int fremantleFCPositive) {
		FremantleFCPositive = fremantleFCPositive;
	}
	
	public int getFremantleFCNegative() {
		return FremantleFCNegative;
	}
	
	public void setFremantleFCNegative(int fremantleFCNegative) {
		FremantleFCNegative = fremantleFCNegative;
	}
	
	public int getFremantleFCTotal() {
		return FremantleFCTotal;
	}
	
	public void setFremantleFCTotal(int fremantleFCTotal) {
		FremantleFCTotal = fremantleFCTotal;
	}
	
	public int getGoldCoastSunsPositive() {
		return GoldCoastSunsPositive;
	}
	
	public void setGoldCoastSunsPositive(int goldCoastSunsPositive) {
		GoldCoastSunsPositive = goldCoastSunsPositive;
	}
	
	public int getGoldCoastSunsNegative() {
		return GoldCoastSunsNegative;
	}
	
	public void setGoldCoastSunsNegative(int goldCoastSunsNegative) {
		GoldCoastSunsNegative = goldCoastSunsNegative;
	}
	
	public int getGoldCoastSunsTotal() {
		return GoldCoastSunsTotal;
	}
	
	public void setGoldCoastSunsTotal(int goldCoastSunsTotal) {
		GoldCoastSunsTotal = goldCoastSunsTotal;
	}
	
	public int getBrisbaneLionsPositive() {
		return BrisbaneLionsPositive;
	}
	
	public void setBrisbaneLionsPositive(int brisbaneLionsPositive) {
		BrisbaneLionsPositive = brisbaneLionsPositive;
	}
	
	public int getBrisbaneLionsNegative() {
		return BrisbaneLionsNegative;
	}
	
	public void setBrisbaneLionsNegative(int brisbaneLionsNegative) {
		BrisbaneLionsNegative = brisbaneLionsNegative;
	}
	
	public int getBrisbaneLionsTotal() {
		return BrisbaneLionsTotal;
	}
	
	public void setBrisbaneLionsTotal(int brisbaneLionsTotal) {
		BrisbaneLionsTotal = brisbaneLionsTotal;
	}
	
	public int getHawthornPositive() {
		return HawthornPositive;
	}
	
	public void setHawthornPositive(int hawthornPositive) {
		HawthornPositive = hawthornPositive;
	}
	
	public int getHawthornNegative() {
		return HawthornNegative;
	}
	
	public void setHawthornNegative(int hawthornNegative) {
		HawthornNegative = hawthornNegative;
	}
	
	public int getHawthornTotal() {
		return HawthornTotal;
	}
	
	public void setHawthornTotal(int hawthornTotal) {
		HawthornTotal = hawthornTotal;
	}
	
	public int getSydneySwansPositive() {
		return SydneySwansPositive;
	}
	
	public void setSydneySwansPositive(int sydneySwansPositive) {
		SydneySwansPositive = sydneySwansPositive;
	}
	
	public int getSydneySwansNegative() {
		return SydneySwansNegative;
	}
	
	public void setSydneySwansNegative(int sydneySwansNegative) {
		SydneySwansNegative = sydneySwansNegative;
	}
	
	public int getSydneySwansTotal() {
		return SydneySwansTotal;
	}
	
	public void setSydneySwansTotal(int sydneySwansTotal) {
		SydneySwansTotal = sydneySwansTotal;
	}

	public int getAFLPositive() {
		return AFLPositive;
	}

	public void setAFLPositive(int aFLPositive) {
		AFLPositive = aFLPositive;
	}

	public int getAFLNegative() {
		return AFLNegative;
	}

	public void setAFLNegative(int aFLNegative) {
		AFLNegative = aFLNegative;
	}

	public int getAFLTotal() {
		return AFLTotal;
	}

	public void setAFLTotal(int AFLTotal) {
		this.AFLTotal = AFLTotal;
	}
	
	public Boolean getTweetsPresent() {
		return tweetsPresent;
	}

	public void setTweetsPresent(Boolean tweetsPresent) {
		this.tweetsPresent = tweetsPresent;
	}

	public String toString() {
		return (suburb + " : " + maxLongitude + " : " + minLongitude + " : " + maxLatitude
			+ " : " + minLatitude + " : " + AdelaideCrowsPositive + " : " + AdelaideCrowsNegative
			+ " : " + AdelaideCrowsTotal + " : " + GeelongCatsPositive + " : " + GeelongCatsNegative
			+ " : " + GeelongCatsTotal + " : " + GWSGiantsPositive + " : " + GWSGiantsNegative
			+ " : " + GWSGiantsTotal + " : " + WesternBulldogsPositive + " : " + WesternBulldogsNegative
			+ " : " + WesternBulldogsTotal + " : " + WestCoastEaglesPositive + " : " + WestCoastEaglesNegative
			+ " : " + WestCoastEaglesTotal + " : " + FremantleFCPositive + " : " + FremantleFCNegative
			+ " : " + FremantleFCTotal + " : " + GoldCoastSunsPositive + " : " + GoldCoastSunsNegative
			+ " : " + GoldCoastSunsTotal + " : " + BrisbaneLionsPositive + " : " + BrisbaneLionsNegative
			+ " : " + BrisbaneLionsTotal + " : " + HawthornPositive + " : " + HawthornNegative
			+ " : " + HawthornTotal + " : " + SydneySwansPositive + " : " + SydneySwansNegative
			+ " : " + SydneySwansTotal + " : " + AFLPositive + " : " + AFLNegative + " : " + AFLTotal);
	}
	
}