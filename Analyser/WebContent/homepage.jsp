<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ include file="/imports.jsp"%>
<html>
<head>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<meta charset="utf-8">
	<title>Tweet Analyser</title>
	<link href="css/buttons.css" rel="stylesheet" type="text/css">
	<link href="css/map.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="js/map.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-latest.js"></script>
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	<%@ include file="/map.jsp"%>
	<%@ include file="/ShowHide.jsp"%>
</head>

<body>
	<div id="navigationBar">
		<table>
			<tr>
				<%if(!adelaideCrowsStats.isEmpty()) { %>
					<td><button id="general" name="adelaide">Adelaide Crows</button></td>
				<% } if(!brisbaneLionsStats.isEmpty()) { %>
					<td><button id="general" name="brisbane">Brisbane Lions</button></td>
				<% } if(!geelongCatsStats.isEmpty()) { %>
					<td><button id="general" name="geelong">Geelong Cats</button></td>
				<% } if(!goldCoastSunsStats.isEmpty()) { %>
					<td><button id="general" name="goldCoast">Gold Coast Suns</button></td>
				<% } if(!gwsGaintsStats.isEmpty()) { %>
					<td><button id="general" name="GWS">GWS Giants</button></td>
				<% } if(!hawthornStats.isEmpty()) { %>
					<td><button id="hawthorn" name="hawthorn">Hawthorn</button></td>
				<% } if(!sydneySwansStats.isEmpty()) { %>
					<td><button id="general" name="sydney">Sydney Swans</button></td>
				<% } if(!westCoastEaglesStats.isEmpty()) { %>
					<td><button id="general" name="westCoast">West Coast Eagles</button></td>
				<% } if(!westernBullDogsStats.isEmpty()) { %>
					<td><button id="general" name="western">Western Bulldogs</button></td>
				<% } if(!aflStats.isEmpty()) { %>
					<td><button id="general" name="afl">Australian Football League</button></td>
				<% } if(!fremantleStats.isEmpty()) { %>
					<td><button id="general" name="fremantle">Fremantle Football Club</button></td>
				<% } else { %>
					<td><button id="general" style="color: #f44336;">Fremantle Football Club</button></td>
				<% } %>
			</tr>
		</table>
	</div><div id="AdelaideCrowsMap" style="float:left"></div>
	<div id="AdelaideCrowsMapDiv" style="float:left">
		<table id="adelaideTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>
			<%
				Iterator<TeamStats> adelaideIterator = adelaideCrowsStats.iterator();
				while (adelaideIterator.hasNext()) {
					TeamStats suburb = adelaideIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="BrisbaneLionsMap" style="float:left;"></div>
	<div id="BrisbaneLionsMapDiv" style="float:left;">
		<table id="brisbaneTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
				<thead style="cursor:pointer">
					<tr>
						<th>Suburb</th>
						<th>Positive Tweets</th>
						<th>Negative Tweets</th>
						<th>Neutral Tweets</th>
					</tr>
				</thead>
				<%
					Iterator<TeamStats> brisbaneIterator = brisbaneLionsStats.iterator();
					while (brisbaneIterator.hasNext()) {
						TeamStats suburb = brisbaneIterator.next();
				%>
				<tr>
					<td><%=suburb.getSuburbName()%></td>
					<td><%=suburb.getPositiveTweets()%></td>
					<td><%=suburb.getNegativeTweets()%></td>
					<td><%=suburb.getNeutralTweets()%></td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	<div id="GeelongCatsMap" style="float:left"></div>
	<div id="GeelongCatsMapDiv" style="float:left; display:none">
		<table id="geelongTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>
			<%
				Iterator<TeamStats> geelongIterator = geelongCatsStats.iterator();
				while (geelongIterator.hasNext()) {
					TeamStats suburb = geelongIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="GoldCoastSunsMap" style="float:left"></div>
	<div id="GoldCoastSunsMapDiv" style="float:left; display:none">
		<table id="goldCoastTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>	
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> goldCoastIterator = goldCoastSunsStats.iterator();
				while (goldCoastIterator.hasNext()) {
					TeamStats suburb = goldCoastIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="GWSGiantsMap" style="float:left"></div>
	<div id="GWSGiantsMapDiv" style="float:left; display:none">
		<table id="gwsTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>	
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> gwsIterator = gwsGaintsStats.iterator();
				while (gwsIterator.hasNext()) {
					TeamStats suburb = gwsIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="HawthornMap" style="float:left"></div>
	<div id="HawthornMapDiv" style="float:left; display:none">
		<table id="hawthornTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>		
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> hawthornIterator = hawthornStats.iterator();
				while (hawthornIterator.hasNext()) {
					TeamStats suburb = hawthornIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="SydneySwansMap" style="float:left"></div>
	<div id="SydneySwansMapDiv" style="float:left; display:none">
		<table id="sydneyTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> sydneyIterator = sydneySwansStats.iterator();
				while (sydneyIterator.hasNext()) {
					TeamStats suburb = sydneyIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="WestCoastEaglesMap" style="float:left"></div>
	<div id="WestCoastEaglesMapDiv" style="float:left; display:none">
		<table id="westCoastTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> westCoastIterator = westCoastEaglesStats.iterator();
				while (westCoastIterator.hasNext()) {
					TeamStats suburb = westCoastIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="WesternBulldogsMap" style="float:left"></div>
	<div id="WesternBulldogsMapDiv" style="float:left; display:none">
		<table id="westernTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> westernIterator = westernBullDogsStats.iterator();
				while (westernIterator.hasNext()) {
					TeamStats suburb = westernIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="AustralianFootballLeagueMap" style="float:left"></div>
	<div id="AustralianFootballLeagueMapDiv" style="float:left; display:none">
		<table id="aflTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> aflIterator = aflStats.iterator();
				while (aflIterator.hasNext()) {
					TeamStats suburb = aflIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<div id="FremantleMap" style="float:left"></div>
	<div id="FremantleMapDiv" style="float:left; display:none">
		<table id="fremantleTable" class="tablesorter">
			<colgroup>
				<col class="Suburb" />
			    <col class="Positive" />
			    <col class="Negative" />
			    <col class="Neutral" />
		  	</colgroup>
			<thead>
				<tr style="cursor:pointer">
					<th>Suburb</th>
					<th>Positive Tweets</th>
					<th>Negative Tweets</th>
					<th>Neutral Tweets</th>
				</tr>
			</thead>	
			<%
				Iterator<TeamStats> fremantleIterator = fremantleStats.iterator();
				while (fremantleIterator.hasNext()) {
					TeamStats suburb = fremantleIterator.next();
			%>
			<tr>
				<td><%=suburb.getSuburbName()%></td>
				<td><%=suburb.getPositiveTweets()%></td>
				<td><%=suburb.getNegativeTweets()%></td>
				<td><%=suburb.getNeutralTweets()%></td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7WmAS_4oynEyFP4N1BHkSVpuldiexbgE&callback=initMap">
		
	</script>
</body>
</html>