<script>
	function initMap() {
		var AdelaideCrowsMap = new google.maps.Map(document.getElementById('AdelaideCrowsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var BrisbaneLionsMap = new google.maps.Map(document.getElementById('BrisbaneLionsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var FremantleMap = new google.maps.Map(document.getElementById('FremantleMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var GeelongCatsMap = new google.maps.Map(document.getElementById('GeelongCatsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var GoldCoastSunsMap = new google.maps.Map(document.getElementById('GoldCoastSunsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var GWSGiantsMap = new google.maps.Map(document.getElementById('GWSGiantsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var HawthornMap = new google.maps.Map(document.getElementById('HawthornMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var SydneySwansMap = new google.maps.Map(document.getElementById('SydneySwansMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var WestCoastEaglesMap = new google.maps.Map(document.getElementById('WestCoastEaglesMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var WesternBulldogsMap = new google.maps.Map(document.getElementById('WesternBulldogsMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		var AustralianFootballLeagueMap = new google.maps.Map(document.getElementById('AustralianFootballLeagueMap'), {
			zoom : 4,
			center : {
				lat : -25.2744,
				lng : 133.7751
			},
			mapTypeId : 'terrain'
		});
		<%
			CouchDbClient dbClientSuburb = new CouchDbClient("suburb-team", true, "http", "115.146.93.181", 5984, "admin", "123456");
			List<Document> suburbDocuments = dbClientSuburb.view("_all_docs").includeDocs(true).query(Document.class);
			for(int index = 0; index < suburbDocuments.size(); index++)
			{
				Suburb suburb = dbClientSuburb.find(Suburb.class, suburbDocuments.get(index).getId());
				if((suburb.getTweetsPresent() != null) && (suburb.getTweetsPresent())) {
					if((suburb.getAdelaideCrowsPositive() >0) || (suburb.getAdelaideCrowsNegative() >0) ||
						(suburb.getAdelaideCrowsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getAdelaideCrowsPositive(), 
								suburb.getAdelaideCrowsNegative(), suburb.getAdelaideCrowsTotal());
						adelaideCrowsStats.add(stats);
		%>
						var rectangle = new google.maps.Rectangle({
						strokeColor : '#FF0000',
						strokeOpacity : 0.8,
						strokeWeight : 2,
						<%
							if(suburb.getAdelaideCrowsPositive() < suburb.getAdelaideCrowsNegative()) {
						%>
								fillColor : '#FF0000',
						<%
							} else if (suburb.getAdelaideCrowsPositive() > suburb.getAdelaideCrowsNegative()) {
						%>
								fillColor : '#FFFF00',
						<%
							} else {
						%>
								fillColor : '#000066',
						<%
							}
						%>
								fillOpacity : 0.35,
								map : AdelaideCrowsMap,
								bounds : {
									north : <%=suburb.getMaxLatitude()%>,
									south : <%=suburb.getMinLatitude()%>,
									east : <%=suburb.getMaxLongitude()%>,
									west : <%=suburb.getMinLongitude()%>
								}
						});
		<%
					}
					if((suburb.getBrisbaneLionsPositive() >0) || (suburb.getBrisbaneLionsNegative() >0) ||
							(suburb.getBrisbaneLionsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getBrisbaneLionsPositive(), 
								suburb.getBrisbaneLionsNegative(), suburb.getBrisbaneLionsTotal());
						brisbaneLionsStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getBrisbaneLionsPositive() < suburb.getBrisbaneLionsNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getBrisbaneLionsPositive() > suburb.getBrisbaneLionsNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : BrisbaneLionsMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					}
					if((suburb.getFremantleFCPositive() >0) || (suburb.getFremantleFCNegative() >0) ||
							(suburb.getFremantleFCTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getFremantleFCPositive(), 
								suburb.getFremantleFCNegative(), suburb.getFremantleFCTotal());
						fremantleStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getFremantleFCPositive() < suburb.getFremantleFCNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getFremantleFCPositive() > suburb.getFremantleFCNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : FremantleMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getGeelongCatsPositive() >0) || (suburb.getGeelongCatsNegative() >0) ||
							(suburb.getGeelongCatsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getGeelongCatsPositive(), 
								suburb.getGeelongCatsNegative(), suburb.getGeelongCatsTotal());
						geelongCatsStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getGeelongCatsPositive() < suburb.getGeelongCatsNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getGeelongCatsPositive() > suburb.getGeelongCatsNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : GeelongCatsMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getGoldCoastSunsPositive() >0) || (suburb.getGoldCoastSunsNegative() >0) ||
							(suburb.getGoldCoastSunsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getGoldCoastSunsPositive(), 
								suburb.getGoldCoastSunsNegative(), suburb.getGoldCoastSunsTotal());
						goldCoastSunsStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getGoldCoastSunsPositive() < suburb.getGoldCoastSunsNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getGoldCoastSunsPositive() > suburb.getGoldCoastSunsNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : GoldCoastSunsMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getGWSGiantsPositive() >0) || (suburb.getGWSGiantsNegative() >0) ||
							(suburb.getGWSGiantsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getGWSGiantsPositive(), 
								suburb.getGWSGiantsNegative(), suburb.getGWSGiantsTotal());
						gwsGaintsStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getGWSGiantsPositive() < suburb.getGWSGiantsNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getGWSGiantsPositive() > suburb.getGWSGiantsNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : GWSGiantsMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getHawthornPositive() >0) || (suburb.getHawthornNegative() >0) ||
							(suburb.getHawthornTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getHawthornPositive(), 
								suburb.getHawthornNegative(), suburb.getHawthornTotal());
						hawthornStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getHawthornPositive() < suburb.getHawthornNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getHawthornPositive() > suburb.getHawthornNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : HawthornMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getSydneySwansPositive() >0) || (suburb.getSydneySwansNegative() >0) ||
							(suburb.getSydneySwansTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getSydneySwansPositive(), 
								suburb.getSydneySwansNegative(), suburb.getSydneySwansTotal());
						sydneySwansStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getSydneySwansPositive() < suburb.getSydneySwansNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getSydneySwansPositive() > suburb.getSydneySwansNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : SydneySwansMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getWestCoastEaglesPositive() >0) || (suburb.getWestCoastEaglesNegative() >0) ||
							(suburb.getWestCoastEaglesTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getWestCoastEaglesPositive(), 
								suburb.getWestCoastEaglesNegative(), suburb.getWestCoastEaglesTotal());
						westCoastEaglesStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getWestCoastEaglesPositive() < suburb.getWestCoastEaglesNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getWestCoastEaglesPositive() > suburb.getWestCoastEaglesNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : WestCoastEaglesMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getWesternBulldogsPositive() >0) || (suburb.getWesternBulldogsNegative() >0) ||
							(suburb.getWesternBulldogsTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getWesternBulldogsPositive(), 
								suburb.getWesternBulldogsNegative(), suburb.getWesternBulldogsTotal());
						westernBullDogsStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getWesternBulldogsPositive() < suburb.getWesternBulldogsNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getWesternBulldogsPositive() > suburb.getWesternBulldogsNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : WesternBulldogsMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					} if((suburb.getAFLPositive() >0) || (suburb.getAFLNegative() >0) ||
							(suburb.getAFLTotal() >0)) {
						TeamStats stats = new TeamStats(suburb.getSuburb(), suburb.getAFLPositive(), 
								suburb.getAFLNegative(), suburb.getAFLTotal());
						aflStats.add(stats);
					%>
						var rectangle = new google.maps.Rectangle({
							strokeColor : '#FF0000',
							strokeOpacity : 0.8,
							strokeWeight : 2,
							<%
								if(suburb.getAFLPositive() < suburb.getAFLNegative()) {
							%>
									fillColor : '#FF0000',
							<%
								} else if (suburb.getAFLPositive() > suburb.getAFLNegative()) {
							%>
									fillColor : '#FFFF00',
							<%
								} else {
							%>
									fillColor : '#000066',
							<%
								}
							%>
							fillOpacity : 0.35,
							map : AustralianFootballLeagueMap,
							bounds : {
								north : <%=suburb.getMaxLatitude()%>,
								south : <%=suburb.getMinLatitude()%>,
								east : <%=suburb.getMaxLongitude()%>,
								west : <%=suburb.getMinLongitude()%>
							}
						});
					<%
					}
				}
			}
		%>				
	}
</script>