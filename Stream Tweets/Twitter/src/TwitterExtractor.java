/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Program to extract the tweets on a real time basis
*/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

import twitter4j.FilterQuery;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

public class TwitterExtractor {

	/** Information necessary for accessing the Twitter API */
	private String consumerKey;
	private String consumerSecret;
	private String accessToken;
	private String accessTokenSecret;

	private String[] keywords;
	private ArrayList<String> tempKeywords;
	private ArrayList<Grid> grids;

	/** The actual Twitter stream. It's set up to collect raw JSON data */
	private TwitterStream twitter;

	public TwitterExtractor() {
		tempKeywords = new ArrayList<String>();
		grids = new ArrayList<Grid>();
	}

	// sets the authentication for twitter
	public void configure() {
		consumerKey = "8pWuUzS8yWN3eAtDeuMfRBWnx";
		consumerSecret = "fPdviCjet8wxpt1Oih03Fh26hM42LB6MwQ0aXzxZz9QfUEmnzh";
		accessToken = "3732157999-kaCsnMBhChtIDH1yqT9dC4GauX7FSdwiO6LWjGs";
		accessTokenSecret = "X0gLewu6uv5FPi57hGWekRl8auC50UX1dC0pTCYBjoxGQ";

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey(consumerKey);
		cb.setOAuthConsumerSecret(consumerSecret);
		cb.setOAuthAccessToken(accessToken);
		cb.setOAuthAccessTokenSecret(accessTokenSecret);
		cb.setJSONStoreEnabled(true);
		cb.setIncludeEntitiesEnabled(true);
		twitter = new TwitterStreamFactory(cb.build()).getInstance();
	}

	public static void main(String[] args) throws TwitterException, IOException {
		TwitterExtractor twitterExtractor = new TwitterExtractor();
		twitterExtractor.configure();
		twitterExtractor.setKeywords();
		twitterExtractor.extractSuburbs();
		twitterExtractor.extractTweets();
	}
	
	// extraction of tweets
	public void extractTweets() {
		CouchDbClient dbClient = new CouchDbClient("tweets", true, "http", "115.146.93.181", 5984, "admin", "123456");
		JSONParser parser = new JSONParser();
		// onStatus is executed each time a tweet is captured
		StatusListener listener = new StatusListener() {
			public void onStatus(Status status) {
				Tweets tweet;
				Response response;
				try {
					String json = "";
					// the json is capture only if one of the location details is present in the tweet
					// coordinates. userLocation, place or timezone
					if (status.getGeoLocation() != null) {
						// Australian coordinates
						Double maxLatitude = -9.1422;
						Double maxLongitude = 159.1092;
						Double minLatitude = -43.7405;
						Double minLongitude = 96.8168;
						Double latitude = status.getGeoLocation().getLatitude();
						Double longitude = status.getGeoLocation().getLongitude();
						if ((latitude >= minLatitude) && (latitude <= maxLatitude) && (longitude >= minLongitude)
								&& (longitude <= maxLongitude)) {
							json = DataObjectFactory.getRawJSON(status);
						}
					} else if ((status.getPlace() != null) || (status.getUser().getLocation() != null)
							|| (status.getUser().getTimeZone() != null)) {
						json = DataObjectFactory.getRawJSON(status);
					}
					if (!json.equals("")) {
						String country = "";
						String city = "";
						double longitude = 0;
						double latitude = 0;
						JSONObject TwitterObject = new JSONObject();
						TwitterObject = (JSONObject) parser.parse(json);
						Long id = (Long) TwitterObject.get("id");
						String text = (String) TwitterObject.get("text");
						text = text.replace("\n", " ");
						JSONObject place = (JSONObject) TwitterObject.get("place");
						if (place != null) {
							country = (String) place.get("country");
							city = (String) place.get("name");
						}
						JSONObject coordinates = (JSONObject) TwitterObject.get("coordinates");
						if (coordinates != null) {
							JSONArray coordinatesArray = (JSONArray) parser
									.parse(coordinates.get("coordinates").toString());
							if (coordinatesArray != null) {
								longitude = Double.parseDouble(coordinatesArray.get(0).toString());
								latitude = Double.parseDouble(coordinatesArray.get(1).toString());
							}
						} 
						JSONObject user = (JSONObject) TwitterObject.get("user");
						String screenName = (String) user.get("screen_name");
						String userLocation = "";
						if(user.get("location") != null) {
							userLocation = (String) user.get("location");
						}
						String userTimeZone = "";
						if(user.get("time_zone") != null) {
							userTimeZone = (String) user.get("time_zone");
						}
						tweet = new Tweets(text, country, city, screenName, userLocation,
								userTimeZone);
						tweet.setId(id.toString());
						boolean locationFound = false;
						// the suburb is found based on one of the lcoation identifiers
						if (longitude != 0.0 && latitude != 0.0) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								boolean isInside = grid.ifInside(longitude, latitude);
								if(isInside) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						if ((tweet.getCity() != null) && !(tweet.getCity().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getCity().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						if ((tweet.getUserLocation() != null) && !(tweet.getUserLocation().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getUserLocation().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						if ((tweet.getUserTimeZone() != null) && !(tweet.getUserTimeZone().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getUserTimeZone().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						// the tweet is stored only if the location is found and is from australia
						if(locationFound) {
							if(!(tweet.getUserTimeZone().contains("usa") || tweet.getUserTimeZone().contains("london")
									|| tweet.getCity().contains("usa") || tweet.getCity().contains("london")
									|| tweet.getUserLocation().contains("usa") || tweet.getUserLocation().contains("london"))) {
								response = dbClient.save(tweet);
							}
						} 						
					}
				} catch (Exception ex) {

				}
			}

			public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
			}

			public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
			}

			public void onException(Exception ex) {
			}

			public void onScrubGeo(long userId, long upToStatusId) {
			}

			public void onStallWarning(StallWarning warning) {
			}
		};
		twitter.addListener(listener);
		FilterQuery query = new FilterQuery().track(keywords);
		twitter.filter(query);
	}
	
	// extracts and stores the suburbs as a list
	public void extractSuburbs() {
		try {
			JSONParser parser = new JSONParser();
			File file = new File("Suburbs.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while(inputKeywords.hasNextLine()) {
				String suburb = inputKeywords.nextLine();
				JSONObject TwitterObject = (JSONObject) parser.parse(suburb);
				String name = (String) TwitterObject.get("name");
				JSONArray coordinates = (JSONArray) TwitterObject.get("coordinates");
				Double maxLongitude = (Double) coordinates.get(2);
				Double minLongitude = (Double) coordinates.get(0);
				Double maxLatitude = (Double) coordinates.get(3);
				Double minLatitude = (Double) coordinates.get(1);
				Grid grid = new Grid(name, maxLongitude, minLongitude, maxLatitude,
						minLatitude);
				grids.add(grid);
			}
			inputKeywords.close();		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// extracts and stores the keywords in a list
	public void setKeywords() {
		try {
			File file = new File("Keywords.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while (inputKeywords.hasNextLine()) {
				String keyword = inputKeywords.nextLine();
				tempKeywords.add(keyword);
			}
			keywords = new String[tempKeywords.size()];
			for (int count = 0; count < tempKeywords.size(); count++) {
				keywords[count] = tempKeywords.get(count);
			}
			inputKeywords.close();
		} catch (Exception ex) {
		}
	}

}