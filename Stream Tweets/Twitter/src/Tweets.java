/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Class used to store the tweets
*/
import java.util.Set;

public class Tweets extends org.lightcouch.Document {

	private String tweetText;
	private String country;
	private String city;
	private Double maxLongitude;
	private Double minLongitude;
	private Double maxLatitude;
	private Double minLatitude;
	private String screenName;
	private String userLocation;
	private String userTimeZone;
	private String suburb;
	private int sentiment;
	private Set<Tweets> tweets;
	
	public Tweets(String tweetText, String country, String city ,
			String screenName, String userLocation, String userTimeZone) {
		this.tweetText = tweetText;
		this.country = country;
		this.city = city;
		this.maxLongitude = maxLongitude;
		this.minLongitude= minLongitude;
		this.minLatitude = minLatitude;
		this.maxLongitude = maxLongitude;
		this.screenName = screenName;
		this.userLocation = userLocation;
		this.userTimeZone = userTimeZone;
		this.sentiment = 0;
	}
	
	public String getTweetText() {
		return tweetText;
	}
	
	public void setTweetText(String tweetText) {
		this.tweetText = tweetText;
	}
	
	public Double getMaxLongitude() {
		return maxLongitude;
	}

	public void setMaxLongitude(Double maxLongitude) {
		this.maxLongitude = maxLongitude;
	}

	public Double getMinLongitude() {
		return minLongitude;
	}

	public void setMinLongitude(Double minLongitude) {
		this.minLongitude = minLongitude;
	}

	public Double getMaxLatitude() {
		return maxLatitude;
	}

	public void setMaxLatitude(Double maxLatitude) {
		this.maxLatitude = maxLatitude;
	}

	public Double getMinLatitude() {
		return minLatitude;
	}

	public void setMinLatitude(Double minLatitude) {
		this.minLatitude = minLatitude;
	}

	public Set<Tweets> getTweets() {
		return tweets;
	}
	
	public void setTweets(Set<Tweets> tweets) {
		this.tweets = tweets;
	}
	
	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	public String getUserTimeZone() {
		return userTimeZone;
	}

	public void setUserTimeZone(String userTimeZone) {
		this.userTimeZone = userTimeZone;
	}
	
	public void setCordinates(Grid grid) {
		this.maxLatitude = grid.getMaxLatitude();
		this.maxLongitude = grid.getMaxLongitude();
		this.minLatitude = grid.getMinLatitude();
		this.minLongitude = grid.getMinLongitude();
	}
	
	public String toString() {
		return (this.getId() + " : " + tweetText + " : " + country + " : " + city + " : " + 
				maxLongitude + " : " + minLongitude + " : " + maxLatitude + " : " + 
				minLatitude + " : " + screenName + " : " + userLocation + " : " + 
				userTimeZone + " : " + suburb + " : " + sentiment);
	}
}
