/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Class used to store the suburb details
*/
public class Grid {

	private String areaName;
	private Double maxLongitude;
	private Double minLongitude;
	private Double maxLatitude;
	private Double minLatitude;
	
	public Grid(String areaName, Double maxLongitude, Double minLongitude,
			Double maxLatitude, Double minLatitude) {
		this.areaName = areaName;
		this.maxLongitude = maxLongitude;
		this.minLongitude = minLongitude;
		this.maxLatitude = maxLatitude;
		this.minLatitude = minLatitude;
	}
	
	public String getAreaName() {
		return areaName;
	}
	
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Double getMaxLongitude() {
		return maxLongitude;
	}

	public void setMaxLongitude(Double maxLongitude) {
		this.maxLongitude = maxLongitude;
	}

	public Double getMinLongitude() {
		return minLongitude;
	}

	public void setMinLongitude(Double minLongitude) {
		this.minLongitude = minLongitude;
	}

	public Double getMaxLatitude() {
		return maxLatitude;
	}

	public void setMaxLatitude(Double maxLatitude) {
		this.maxLatitude = maxLatitude;
	}

	public Double getMinLatitude() {
		return minLatitude;
	}

	public void setMinLatitude(Double minLatitude) {
		this.minLatitude = minLatitude;
	}
	
	public boolean ifInside(Double longitude, Double latitude) {
		if((latitude >= minLatitude) && (latitude <= maxLatitude)
				&& (longitude >= minLongitude) && (longitude <= maxLongitude)) {
			return true;
		} else {
			return false;
		}
	}
}
