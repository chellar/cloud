{ "name":"Acton" ,"coordinates": [148.9613,-35.3189,149.1279,-35.1919]}
{ "name":"Ainslie" ,"coordinates": [149.1359,-35.2762,149.1605,-35.2504]}
{ "name":"Amaroo" ,"coordinates": [149.1152,-35.1791,149.1395,-35.1586]}
{ "name":"Aranda" ,"coordinates": [149.0736,-35.2653,149.0893,-35.2505]}
{ "name":"Banks" ,"coordinates": [149.0908,-35.4803,149.1110,-35.4634]}
{ "name":"Barton" ,"coordinates": [149.1286,-35.3168,149.1544,-35.2928]}
{ "name":"Belconnen" ,"coordinates": [149.0563,-35.2505,149.0803,-35.2217]}
{ "name":"Bonner" ,"coordinates": [149.1310,-35.1669,149.1565,-35.1477]}
{ "name":"Bonython" ,"coordinates": [149.0643,-35.4465,149.0913,-35.4238]}
{ "name":"Braddon" ,"coordinates": [149.1299,-35.2791,149.1435,-35.2605]}
{ "name":"Bruce" ,"coordinates": [149.0734,-35.2570,149.1140,-35.2310]}
{ "name":"Calwell" ,"coordinates": [149.0913,-35.4532,149.1244,-35.4326]}
{ "name":"Campbell" ,"coordinates": [149.1409,-35.3061,149.1777,-35.2717]}
{ "name":"Casey" ,"coordinates": [149.0839,-35.1786,149.1048,-35.1536]}
{ "name":"Chapman" ,"coordinates": [149.0233,-35.3652,149.0498,-35.3440]}
{ "name":"Charnwood" ,"coordinates": [149.0234,-35.2075,149.0447,-35.1936]}
{ "name":"Chifley" ,"coordinates": [149.0689,-35.3602,149.0865,-35.3476]}
{ "name":"Chisholm" ,"coordinates": [149.1121,-35.4316,149.1398,-35.4121]}
{ "name":"Conder" ,"coordinates": [149.0909,-35.4720,149.1225,-35.4433]}
{ "name":"Cook" ,"coordinates": [149.0544,-35.2674,149.0746,-35.2534]}
{ "name":"Crace" ,"coordinates": [149.0949,-35.2106,149.1172,-35.1958]}
{ "name":"Curtin" ,"coordinates": [149.0629,-35.3386,149.0920,-35.3123]}
{ "name":"Deakin" ,"coordinates": [149.0873,-35.3284,149.1212,-35.3094]}
{ "name":"Dickson" ,"coordinates": [149.1323,-35.2610,149.1555,-35.2471]}
{ "name":"Downer" ,"coordinates": [149.1343,-35.2489,149.1541,-35.2363]}
{ "name":"Duffy" ,"coordinates": [149.0224,-35.3444,149.0446,-35.3253]}
{ "name":"Dunlop" ,"coordinates": [149.0071,-35.2055,149.0332,-35.1843]}
{ "name":"Evatt" ,"coordinates": [149.0541,-35.2228,149.0826,-35.1999]}
{ "name":"Fadden" ,"coordinates": [149.1052,-35.4163,149.1300,-35.3900]}
{ "name":"Farrer" ,"coordinates": [149.0930,-35.3843,149.1165,-35.3683]}
{ "name":"Fisher" ,"coordinates": [149.0477,-35.3669,149.0689,-35.3539]}
{ "name":"Florey" ,"coordinates": [149.0349,-35.2359,149.0624,-35.2177]}
{ "name":"Flynn" ,"coordinates": [149.0340,-35.2177,149.0534,-35.1947]}
{ "name":"Forde" ,"coordinates": [149.1380,-35.1800,149.1566,-35.1540]}
{ "name":"Forrest" ,"coordinates": [149.1149,-35.3233,149.1354,-35.3107]}
{ "name":"Franklin" ,"coordinates": [149.1323,-35.2087,149.1514,-35.1871]}
{ "name":"Fraser" ,"coordinates": [149.0310,-35.2012,149.0589,-35.1826]}
{ "name":"Garran" ,"coordinates": [149.0922,-35.3507,149.1244,-35.3313]}
{ "name":"Gilmore" ,"coordinates": [149.1274,-35.4251,149.1448,-35.4051]}
{ "name":"Giralang" ,"coordinates": [149.0844,-35.2195,149.1089,-35.2014]}
{ "name":"Gordon" ,"coordinates": [149.0721,-35.4752,149.0934,-35.4377]}
{ "name":"Gowrie" ,"coordinates": [149.1022,-35.4208,149.1195,-35.4030]}
{ "name":"Greenway" ,"coordinates": [149.0514,-35.4343,149.0783,-35.3963]}
{ "name":"Griffith" ,"coordinates": [149.1268,-35.3369,149.1520,-35.3161]}
{ "name":"Gungahlin" ,"coordinates": [149.1196,-35.2018,149.1557,-35.1737]}
{ "name":"Hackett" ,"coordinates": [149.1541,-35.2601,149.1705,-35.2395]}
{ "name":"Hall" ,"coordinates": [149.0556,-35.1809,149.0767,-35.1592]}
{ "name":"Harrison" ,"coordinates": [149.1474,-35.2096,149.1672,-35.1846]}
{ "name":"Hawker" ,"coordinates": [149.0280,-35.2552,149.0456,-35.2396]}
{ "name":"Higgins" ,"coordinates": [149.0167,-35.2422,149.0349,-35.2242]}
{ "name":"Holder" ,"coordinates": [149.0373,-35.3424,149.0538,-35.3257]}
{ "name":"Holt" ,"coordinates": [148.9959,-35.2321,149.0309,-35.2170]}
{ "name":"Hughes" ,"coordinates": [149.0863,-35.3394,149.1048,-35.3265]}
{ "name":"Hume" ,"coordinates": [149.1457,-35.4107,149.1908,-35.3655]}
{ "name":"Isaacs" ,"coordinates": [149.1054,-35.3826,149.1252,-35.3541]}
{ "name":"Isabella Plains" ,"coordinates": [149.0783,-35.4377,149.1064,-35.4208]}
{ "name":"Kalee" ,"coordinates": [149.0920,-35.2437,149.1251,-35.2097]}
{ "name":"Kambah" ,"coordinates": [149.0288,-35.4034,149.0933,-35.3694]}
{ "name":"Kingston" ,"coordinates": [149.1367,-35.3214,149.1544,-35.3076]}
{ "name":"Latham" ,"coordinates": [149.0191,-35.2279,149.0433,-35.2055]}
{ "name":"Lawson" ,"coordinates": [149.0742,-35.2348,149.1007,-35.2162]}
{ "name":"Lyneham" ,"coordinates": [149.1140,-35.2605,149.1513,-35.2256]}
{ "name":"Lyons" ,"coordinates": [149.0637,-35.3489,149.0841,-35.3314]}
{ "name":"Macarthur" ,"coordinates": [149.1239,-35.4120,149.1419,-35.3980]}
{ "name":"Macgregor" ,"coordinates": [148.9968,-35.2195,149.0272,-35.1983]}
{ "name":"Macquarie" ,"coordinates": [149.0540,-35.2572,149.0758,-35.2438]}
{ "name":"Mawson" ,"coordinates": [149.0917,-35.3719,149.1086,-35.3532]}
{ "name":"McKellar" ,"coordinates": [149.0689,-35.2259,149.0869,-35.2092]}
{ "name":"Melba" ,"coordinates": [149.0433,-35.2200,149.0653,-35.2005]}
{ "name":"Mitchell" ,"coordinates": [149.1100,-35.2314,149.1474,-35.1975]}
{ "name":"Monash" ,"coordinates": [149.0747,-35.4238,149.1033,-35.4067]}
{ "name":"Narrabundah" ,"coordinates": [149.1352,-35.3457,149.1630,-35.3245]}
{ "name":"Ngunnawal" ,"coordinates": [149.0981,-35.1862,149.1286,-35.1565]}
{ "name":"Nicholls" ,"coordinates": [149.0756,-35.2012,149.1196,-35.1756]}
{ "name":"Oaks Estate" ,"coordinates": [149.1871,-35.3459,149.2332,-35.3318]}
{ "name":"O'Connor" ,"coordinates": [149.0893,-35.2689,149.1308,-35.2435]}
{ "name":"O'Malley" ,"coordinates": [149.1003,-35.3624,149.1274,-35.3451]}
{ "name":"Oxley" ,"coordinates": [149.0731,-35.4154,149.0868,-35.4029]}
{ "name":"Page" ,"coordinates": [149.0410,-35.2438,149.0579,-35.2316]}
{ "name":"Palmerston" ,"coordinates": [149.1081,-35.2003,149.1308,-35.1862]}
{ "name":"Parkes" ,"coordinates": [149.1193,-35.3124,149.1398,-35.2943]}
{ "name":"Pearce" ,"coordinates": [149.0730,-35.3704,149.0926,-35.3557]}
{ "name":"Phillip" ,"coordinates": [149.0823,-35.3573,149.1005,-35.3366]}
{ "name":"Pialligo" ,"coordinates": [149.1513,-35.3422,149.2636,-35.2056]}
{ "name":"Red Hill" ,"coordinates": [149.1018,-35.3479,149.1388,-35.3187]}
{ "name":"Reid" ,"coordinates": [149.1270,-35.2956,149.1472,-35.2761]}
{ "name":"Richardson" ,"coordinates": [149.1033,-35.4361,149.1298,-35.4163]}
{ "name":"Rivett" ,"coordinates": [149.0287,-35.3539,149.0489,-35.3415]}
{ "name":"Scullin" ,"coordinates": [149.0301,-35.2411,149.0486,-35.2279]}
{ "name":"Spence" ,"coordinates": [149.0573,-35.2077,149.0736,-35.1900]}
{ "name":"Stirling" ,"coordinates": [149.0417,-35.3562,149.0573,-35.3424]}
{ "name":"Symonston" ,"coordinates": [149.1185,-35.3967,149.2016,-35.3020]}
{ "name":"Tharwa" ,"coordinates": [148.8100,-35.5343,149.0830,-35.2185]}
{ "name":"Theodore" ,"coordinates": [149.1077,-35.4612,149.1308,-35.4361]}
{ "name":"Torrens" ,"coordinates": [149.0807,-35.3782,149.0933,-35.3644]}
{ "name":"Turner" ,"coordinates": [149.1132,-35.2756,149.1323,-35.2604]}
{ "name":"Wanniassa" ,"coordinates": [149.0723,-35.4104,149.1064,-35.3843]}
{ "name":"Waramanga" ,"coordinates": [149.0520,-35.3590,149.0701,-35.3455]}
{ "name":"Watson" ,"coordinates": [149.1437,-35.2471,149.1727,-35.2225]}
{ "name":"Weetangera" ,"coordinates": [149.0436,-35.2584,149.0563,-35.2426]}
{ "name":"Weston" ,"coordinates": [149.0489,-35.3484,149.0696,-35.3204]}
{ "name":"Yarralumla" ,"coordinates": [149.0711,-35.3155,149.1532,-35.2840]}
{ "name":"Brunswick" ,"coordinates": [144.9485,-37.7793,144.9770,-37.7542]}
{ "name":"Coburg" ,"coordinates": [144.9457,-37.7573,144.9829,-37.7325]}
{ "name":"Pascoe Vale South" ,"coordinates": [144.9269,-37.7533,144.9491,-37.7336]}
{ "name":"Alphington" ,"coordinates": [145.0143,-37.7838,145.0371,-37.7651]}
{ "name":"Northcote" ,"coordinates": [144.9802,-37.7856,145.0178,-37.7573]}
{ "name":"Thornbury" ,"coordinates": [144.9792,-37.7666,145.0323,-37.7503]}
{ "name":"Ascot Vale" ,"coordinates": [144.8958,-37.7896,144.9371,-37.7700]}
{ "name":"Essendon" ,"coordinates": [144.8889,-37.7658,144.9351,-37.7346]}
{ "name":"Flemington" ,"coordinates": [144.9176,-37.7890,144.9404,-37.7770]}
{ "name":"Moonee Ponds" ,"coordinates": [144.9010,-37.7738,144.9384,-37.7569]}
{ "name":"Carlton" ,"coordinates": [144.9573,-37.8079,144.9757,-37.7923]}
{ "name":"Docklands" ,"coordinates": [144.9199,-37.8254,144.9560,-37.8095]}
{ "name":"East Melbourne" ,"coordinates": [144.9674,-37.8297,144.9914,-37.8077]}
{ "name":"Kensington" ,"coordinates": [144.9129,-37.8023,144.9378,-37.7874]}
{ "name":"North Melbourne" ,"coordinates": [144.9357,-37.8135,144.9595,-37.7876]}
{ "name":"Parkville" ,"coordinates": [144.9371,-37.8016,144.9650,-37.7754]}
{ "name":"Southbank" ,"coordinates": [144.9377,-37.8343,144.9879,-37.8192]}
{ "name":"South Yarra" ,"coordinates": [144.9723,-37.8507,144.9879,-37.8283]}
{ "name":"West Melbourne" ,"coordinates": [144.9055,-37.8233,144.9460,-37.7982]}
{ "name":"Albert Park" ,"coordinates": [144.9427,-37.8575,144.9806,-37.8323]}
{ "name":"Elwood" ,"coordinates": [144.9745,-37.8917,144.9970,-37.8707]}
{ "name":"Port Melbourne" ,"coordinates": [144.9125,-37.8474,144.9498,-37.8306]}
{ "name":"South Melbourne" ,"coordinates": [144.9408,-37.8422,144.9718,-37.8258]}
{ "name":"St Kilda" ,"coordinates": [144.9657,-37.8763,144.9933,-37.8502]}
{ "name":"Armadale" ,"coordinates": [145.0105,-37.8663,145.0301,-37.8493]}
{ "name":"Toorak" ,"coordinates": [145.0032,-37.8516,145.0327,-37.8305]}
{ "name":"Abbotsford" ,"coordinates": [144.9914,-37.8121,145.0158,-37.7956]}
{ "name":"Collingwood" ,"coordinates": [144.9826,-37.8097,144.9936,-37.7942]}
{ "name":"Fitzroy" ,"coordinates": [144.9732,-37.8089,144.9851,-37.7930]}
{ "name":"Richmond" ,"coordinates": [144.9879,-37.8345,145.0284,-37.8097]}
{ "name":"Ashburton" ,"coordinates": [145.0653,-37.8759,145.0937,-37.8581]}
{ "name":"Balwyn" ,"coordinates": [145.0596,-37.8191,145.1049,-37.8008]}
{ "name":"Camberwell" ,"coordinates": [145.0540,-37.8504,145.0982,-37.8214]}
{ "name":"Hawthorn" ,"coordinates": [145.0144,-37.8449,145.0463,-37.8121]}
{ "name":"Kew" ,"coordinates": [144.9993,-37.8174,145.0625,-37.7856]}
{ "name":"Bulleen" ,"coordinates": [145.0671,-37.7826,145.1047,-37.7514]}
{ "name":"Doncaster" ,"coordinates": [145.0987,-37.7988,145.1451,-37.7669]}
{ "name":"Templestowe" ,"coordinates": [145.1184,-37.7786,145.1777,-37.7339]}
{ "name":"Blackburn" ,"coordinates": [145.1363,-37.8340,145.1696,-37.7972]}
{ "name":"Box Hill" ,"coordinates": [145.1102,-37.8458,145.1395,-37.8126]}
{ "name":"Burwood" ,"coordinates": [145.0951,-37.8576,145.1335,-37.8360]}
{ "name":"Beaumaris" ,"coordinates": [145.0216,-37.9964,145.0544,-37.9675]}
{ "name":"Brighton" ,"coordinates": [145.0040,-37.9330,145.0344,-37.8998]}
{ "name":"Hampton" ,"coordinates": [144.9921,-37.9464,145.0393,-37.9281]}
{ "name":"Bentleigh" ,"coordinates": [145.0483,-37.9388,145.0852,-37.9056]}
{ "name":"Caulfield" ,"coordinates": [144.9980,-37.8873,145.0496,-37.8602]}
{ "name":"Elsternwick" ,"coordinates": [144.9967,-37.9005,145.0163,-37.8769]}
{ "name":"Hughesdale" ,"coordinates": [145.0742,-37.9095,145.0882,-37.8872]}
{ "name":"Murrumbeena" ,"coordinates": [145.0604,-37.9081,145.0782,-37.8846]}
{ "name":"Ormond" ,"coordinates": [145.0253,-37.9096,145.0547,-37.8863]}
{ "name":"Aspendale Gardens" ,"coordinates": [145.1006,-38.0338,145.1408,-38.0086]}
{ "name":"Braeside" ,"coordinates": [145.1033,-38.0149,145.1447,-37.9723]}
{ "name":"Mentone" ,"coordinates": [145.0517,-37.9949,145.0953,-37.9735]}
{ "name":"Mordialloc" ,"coordinates": [145.0687,-38.0119,145.1093,-37.9822]}
{ "name":"Malvern" ,"coordinates": [145.0383,-37.8930,145.0922,-37.8639]}
{ "name":"Banyule" ,"coordinates": [145.0278,-37.7851,145.1437,-37.6826]}
{ "name":"Darebin" ,"coordinates": [144.9703,-37.7557,145.0754,-37.6910]}
{ "name":"Nillumbik" ,"coordinates": [145.0789,-37.7417,145.5800,-37.4093]}
{ "name":"Whittlesea" ,"coordinates": [144.8807,-37.7000,145.2658,-37.2629]}
{ "name":"Keilor" ,"coordinates": [144.8016,-37.7761,144.9347,-37.6982]}
{ "name":"Macedon Ranges" ,"coordinates": [144.4577,-37.5677,144.9212,-37.1751]}
{ "name":"Moreland" ,"coordinates": [144.8862,-37.7360,144.9853,-37.6909]}
{ "name":"Sunbury" ,"coordinates": [144.6236,-37.6649,144.8705,-37.4819]}
{ "name":"Tullamarine" ,"coordinates": [144.7600,-37.7104,144.9778,-37.5021]}
{ "name":"Knox" ,"coordinates": [145.1908,-37.9649,145.3476,-37.8330]}
{ "name":"Manningham" ,"coordinates": [145.1678,-37.8118,145.2970,-37.7024]}
{ "name":"Maroondah" ,"coordinates": [145.2133,-37.8439,145.3187,-37.7618]}
{ "name":"Whitehorse" ,"coordinates": [145.1569,-37.8652,145.2167,-37.8012]}
{ "name":"Yarra Ranges" ,"coordinates": [145.2869,-37.9750,145.8784,-37.5260]}
{ "name":"Cardinia" ,"coordinates": [145.3640,-38.3325,145.7651,-37.8577]}
{ "name":"Dandenong" ,"coordinates": [145.0795,-38.0777,145.2519,-37.9240]}
{ "name":"Brimbank" ,"coordinates": [144.7444,-37.8228,144.8559,-37.6629]}
{ "name":"Hobsons Bay" ,"coordinates": [144.7514,-37.9000,144.9155,-37.8138]}
{ "name":"Maribyrnong" ,"coordinates": [144.8392,-37.8270,144.9165,-37.7558]}
{ "name":"Melton" ,"coordinates": [144.3336,-37.8105,144.7682,-37.5464]}
{ "name":"Wyndham" ,"coordinates": [144.4441,-38.0046,144.8274,-37.7810]}
{ "name":"Frankston" ,"coordinates": [145.0979,-38.2007,145.2396,-38.0674]}
{ "name":"Mornington Peninsula" ,"coordinates": [144.6514,-38.5030,145.2617,-38.1624]}
{ "name":"Ballarat" ,"coordinates": [143.6553,-37.6835,143.9510,-37.4548]}
{ "name":"Bendigo" ,"coordinates": [144.1701,-36.8529,144.4240,-36.6253]}
{ "name":"Heathcote" ,"coordinates": [143.8346,-37.4589,144.8536,-36.6473]}
{ "name":"Loddon" ,"coordinates": [143.3152,-36.8667,144.6743,-35.9059]}
{ "name":"Barwon" ,"coordinates": [143.6221,-38.4668,144.2961,-37.7812]}
{ "name":"Geelong" ,"coordinates": [144.2025,-38.2505,144.5919,-37.8011]}
{ "name":"Upper Goulburn Valley" ,"coordinates": [144.5304,-37.8280,146.6474,-36.4246]}
{ "name":"Wangaratta" ,"coordinates": [145.7246,-37.1626,146.7258,-35.9579]}
{ "name":"Wodonga" ,"coordinates": [146.5124,-37.2755,148.2207,-35.9285]}
{ "name":"Baw Baw" ,"coordinates": [145.7008,-38.3601,146.5162,-37.5799]}
{ "name":"Gippsland" ,"coordinates": [147.0833,-38.0788,149.9767,-36.6124]}
{ "name":"Latrobe Valley" ,"coordinates": [146.1922,-38.5365,146.7120,-37.9634]}
{ "name":"Wellington" ,"coordinates": [146.3113,-38.7909,147.9722,-37.1200]}
{ "name":"Grampians" ,"coordinates": [140.9631,-37.8366,143.5110,-35.2909]}
{ "name":"Mildura" ,"coordinates": [140.9617,-35.7486,142.7430,-33.9804]}
{ "name":"Murray River" ,"coordinates": [142.5170,-36.4999,144.4182,-34.5464]}
{ "name":"Shepparton" ,"coordinates": [144.2593,-36.7626,146.2465,-35.8020]}
{ "name":"Moira" ,"coordinates": [144.8298,-36.2992,146.2465,-35.8020]}
{ "name":"Campaspe" ,"coordinates": [144.2593,-36.7626,145.1866,-35.8841]}
{ "name":"Gosford" ,"coordinates": [150.9841,-33.5706,151.4850,-33.1100]}
{ "name":"Wyong" ,"coordinates": [151.2079,-33.4009,151.6305,-33.0436]}
{ "name":"Baulkham Hills" ,"coordinates": [150.9306,-33.7730,151.0669,-33.6886]}
{ "name":"Dural" ,"coordinates": [150.8697,-33.7203,151.1556,-33.3645]}
{ "name":"Hawkesbury" ,"coordinates": [150.3567,-33.6203,151.1358,-32.9961]}
{ "name":"Rouse Hill" ,"coordinates": [150.8219,-33.7084,150.9696,-33.5544]}
{ "name":"Blacktown" ,"coordinates": [150.8559,-33.8102,150.9669,-33.7328]}
{ "name":"Mount Druitt" ,"coordinates": [150.7596,-33.8340,150.9184,-33.7142]}
{ "name":"Bankstown" ,"coordinates": [150.9660,-33.9836,151.0773,-33.8820]}
{ "name":"Canterbury" ,"coordinates": [151.0381,-33.9459,151.1553,-33.8964]}
{ "name":"Hurstville" ,"coordinates": [151.0333,-34.0034,151.1222,-33.9403]}
{ "name":"Kogarah" ,"coordinates": [151.0895,-34.0060,151.1680,-33.9262]}
{ "name":"Canada Bay" ,"coordinates": [151.0780,-33.8743,151.1648,-33.8228]}
{ "name":"Leichhardt" ,"coordinates": [151.1446,-33.8901,151.1966,-33.8456]}
{ "name":"Strathfield" ,"coordinates": [151.0567,-33.9163,151.1511,-33.8500]}
{ "name":"Manly" ,"coordinates": [151.2321,-33.8239,151.3076,-33.7779]}
{ "name":"Pittwater" ,"coordinates": [151.2100,-33.7147,151.3430,-33.5719]}
{ "name":"Warringah" ,"coordinates": [151.1608,-33.7924,151.3177,-33.6145]}
{ "name":"Chatswood" ,"coordinates": [151.1423,-33.8434,151.2330,-33.7785]}
{ "name":"Hornsby" ,"coordinates": [151.0579,-33.7392,151.2471,-33.5073]}
{ "name":"North Sydney" ,"coordinates": [151.1874,-33.8536,151.2689,-33.8033]}
{ "name":"Goulburn" ,"coordinates": [147.7108,-35.3671,150.3335,-33.8878]}
{ "name":"Queanbeyan" ,"coordinates": [149.0780,-36.0092,150.2157,-34.9803]}
{ "name":"Snowy Mountains" ,"coordinates": [148.2007,-37.2629,149.5837,-35.6457]}
{ "name":"South Coast" ,"coordinates": [149.3166,-37.5051,150.3791,-35.4971]}
{ "name":"Bathurst" ,"coordinates": [149.1169,-34.2058,150.1958,-32.9486]}
{ "name":"Lachlan Valley" ,"coordinates": [146.0540,-34.3172,149.1355,-31.9928]}
{ "name":"Lithgow" ,"coordinates": [148.9971,-33.7607,150.6198,-31.7787]}
{ "name":"Orange" ,"coordinates": [148.3276,-33.8686,149.4221,-32.7160]}
{ "name":"Clarence Valley" ,"coordinates": [152.1683,-30.1949,153.3948,-28.9700]}
{ "name":"Coffs Harbour" ,"coordinates": [152.3095,-30.5671,153.2696,-29.8971]}
{ "name":"Dubbo" ,"coordinates": [147.4949,-33.0469,150.1106,-30.6056]}
{ "name":"Lower Hunter" ,"coordinates": [150.5483,-33.1390,151.9187,-32.0414]}
{ "name":"Maitland" ,"coordinates": [151.4031,-32.8105,151.7440,-32.6057]}
{ "name":"Port Stephens" ,"coordinates": [151.5935,-32.8526,152.3369,-32.5652]}
{ "name":"Upper Hunter" ,"coordinates": [149.7916,-32.8487,151.5940,-31.5537]}
{ "name":"Wollongong" ,"coordinates": [150.8299,-34.4492,151.0662,-34.0934]}
{ "name":"Great Lakes" ,"coordinates": [151.6498,-32.6158,152.5709,-32.0652]}
{ "name":"Albury" ,"coordinates": [146.5720,-36.1160,147.8192,-35.4485]}
{ "name":"Lower Murray" ,"coordinates": [141.0017,-35.0920,145.2595,-32.7485]}
{ "name":"Lake Macquarie" ,"coordinates": [151.5959,-33.1771,151.7379,-32.9199]}
{ "name":"Newcastle" ,"coordinates": [151.6008,-32.9630,151.8788,-32.7798]}
{ "name":"Armidale" ,"coordinates": [150.9343,-31.6381,152.4281,-29.9400]}
{ "name":"Richmond Valley" ,"coordinates": [153.1581,-29.2637,153.6387,-28.4612]}
{ "name":"Tweed Valley" ,"coordinates": [153.1067,-28.5428,153.5863,-28.1570]}
{ "name":"Wagga Wagga" ,"coordinates": [146.3542,-35.5759,148.5831,-34.1065]}
{ "name":"Shoalhaven" ,"coordinates": [149.9761,-35.5684,150.8498,-34.6172]}
{ "name":"Southern Highlands" ,"coordinates": [150.0452,-34.7572,150.7466,-34.2125]}
{ "name":"Litchfield" ,"coordinates": [130.8590,-12.8619,131.3967,-12.0010]}
{ "name":"Alice Springs" ,"coordinates": [129.0005,-25.9995,137.9991,-17.9452]}
{ "name":"Barkly" ,"coordinates": [131.6461,-21.9986,138.0012,-16.5022]}
{ "name":"East Arnhem" ,"coordinates": [134.6484,-14.3226,136.9789,-11.0009]}
{ "name":"Katherine" ,"coordinates": [129.0004,-18.6827,138.0012,-12.9903]}
{ "name":"Capalaba" ,"coordinates": [153.1187,-27.5684,153.2614,-27.4649]}
{ "name":"Cleveland" ,"coordinates": [153.1715,-27.7408,153.5467,-27.0220]}
{ "name":"Wynnum" ,"coordinates": [153.0879,-27.4933,153.2710,-27.3207]}
{ "name":"Bald Hills" ,"coordinates": [152.9752,-27.4143,153.0496,-27.2787]}
{ "name":"Chermside" ,"coordinates": [152.9970,-27.4224,153.0618,-27.3524]}
{ "name":"Nundah" ,"coordinates": [153.0472,-27.4457,153.1605,-27.3279]}
{ "name":"Sandgate" ,"coordinates": [153.0133,-27.3678,153.0882,-27.2787]}
{ "name":"Carindale" ,"coordinates": [153.0635,-27.5365,153.1262,-27.4576]}
{ "name":"Holland Park" ,"coordinates": [152.9900,-27.5413,153.0847,-27.4807]}
{ "name":"Mt Gravatt" ,"coordinates": [153.0589,-27.6066,153.1918,-27.5157]}
{ "name":"Nathan" ,"coordinates": [153.0120,-27.5755,153.0680,-27.5137]}
{ "name":"Rocklea" ,"coordinates": [152.9693,-27.6604,153.0975,-27.5296]}
{ "name":"Sunnybank" ,"coordinates": [153.0376,-27.6182,153.1058,-27.5697]}
{ "name":"Centenary" ,"coordinates": [152.8955,-27.5687,152.9655,-27.5281]}
{ "name":"Kenmore" ,"coordinates": [152.8109,-27.6021,152.9737,-27.4427]}
{ "name":"Sherwood" ,"coordinates": [152.9550,-27.5512,153.0206,-27.4868]}
{ "name":"The Gap" ,"coordinates": [152.7986,-27.4944,153.0007,-27.3933]}
{ "name":"Forest Lake" ,"coordinates": [152.8946,-27.6365,152.9984,-27.5446]}
{ "name":"Ipswich Hinterland" ,"coordinates": [152.1340,-28.3390,152.9517,-26.9902]}
{ "name":"Springfield" ,"coordinates": [152.8138,-27.7081,152.9410,-27.5796]}
{ "name":"Beaudesert" ,"coordinates": [152.6478,-28.3640,153.1428,-27.8629]}
{ "name":"Beenleigh" ,"coordinates": [153.1529,-27.8062,153.2450,-27.6786]}
{ "name":"Browns Plains" ,"coordinates": [152.9247,-27.7746,153.1263,-27.6287]}
{ "name":"Jimboomba" ,"coordinates": [152.7993,-27.9386,153.1833,-27.6661]}
{ "name":"Loganlea" ,"coordinates": [153.1025,-27.7207,153.2905,-27.6342]}
{ "name":"Springwood" ,"coordinates": [153.0680,-27.6774,153.1836,-27.5873]}
{ "name":"Moreton Bay" ,"coordinates": [152.0734,-27.2635,153.2076,-26.4519]}
{ "name":"Cairns" ,"coordinates": [144.7408,-18.5783,146.3587,-15.9028]}
{ "name":"Darling Downs" ,"coordinates": [146.8631,-29.1779,152.4926,-24.8789]}
{ "name":"Rockhampton" ,"coordinates": [149.3676,-23.8537,151.0867,-21.9152]}
{ "name":"Gladstone" ,"coordinates": [148.8131,-25.9661,152.7184,-23.2455]}
{ "name":"Central Highlands" ,"coordinates": [146.5725,-25.4725,150.0667,-22.6543]}
{ "name":"Broadbeach" ,"coordinates": [153.3901,-28.1162,153.4607,-28.0158]}
{ "name":"Coolangatta" ,"coordinates": [153.4321,-28.1792,153.5522,-28.0953]}
{ "name":"Gold Coast" ,"coordinates": [153.3380,-27.9568,153.4132,-27.8672]}
{ "name":"Mudgeeraba" ,"coordinates": [153.2943,-28.2503,153.4579,-28.0495]}
{ "name":"Nerang" ,"coordinates": [153.2453,-28.0839,153.3958,-27.9240]}
{ "name":"Robina" ,"coordinates": [153.3535,-28.1024,153.4256,-28.0310]}
{ "name":"Southport" ,"coordinates": [153.3411,-28.0002,153.4211,-27.9449]}
{ "name":"Surfers Paradise" ,"coordinates": [153.3640,-28.0293,153.4346,-27.9313]}
{ "name":"Bowen Basin" ,"coordinates": [146.0315,-23.5560,150.4420,-19.7055]}
{ "name":"Mackay" ,"coordinates": [148.1489,-21.7710,149.9364,-20.3928]}
{ "name":"Whitsunday" ,"coordinates": [148.2517,-20.7323,149.1782,-20.0142]}
{ "name":"Buderim" ,"coordinates": [152.9988,-26.7439,153.1222,-26.6505]}
{ "name":"Caloundra" ,"coordinates": [153.0280,-26.8533,153.1514,-26.6789]}
{ "name":"Maroochy" ,"coordinates": [153.0428,-26.6965,153.1356,-26.4957]}
{ "name":"Nambour" ,"coordinates": [152.7630,-26.6803,153.0963,-26.1371]}
{ "name":"Noosa" ,"coordinates": [152.9830,-26.5105,153.1199,-26.3596]}
{ "name":"Sunshine Coast Hinterland" ,"coordinates": [152.5509,-26.9848,153.1133,-26.4636]}
{ "name":"Toowoomba" ,"coordinates": [151.7665,-27.9697,152.3823,-27.3493]}
{ "name":"Townsville" ,"coordinates": [144.2852,-22.1048,147.6633,-18.3137]}
{ "name":"Wide Bay" ,"coordinates": [150.3696,-26.9479,153.3604,-24.3921]}
{ "name":"Barossa" ,"coordinates": [138.2252,-34.8153,139.1719,-34.1876]}
{ "name":"Yorke Peninsula" ,"coordinates": [136.4414,-35.3782,138.1278,-33.8180]}
{ "name":"Hobart" ,"coordinates": [147.0267,-43.1213,147.9369,-42.6554]}
{ "name":"Launceston" ,"coordinates": [146.9720,-41.5102,147.3052,-41.3352]}
{ "name":"Meander Valley" ,"coordinates": [145.9513,-41.9746,147.1129,-41.0633]}
{ "name":"Devonport" ,"coordinates": [145.8520,-41.6847,146.7621,-41.0925]}
{ "name":"Mandurah" ,"coordinates": [115.6068,-32.8019,116.0315,-32.4446]}
{ "name":"Bunbury" ,"coordinates": [115.4378,-33.6728,116.4126,-32.7552]}
{ "name":"Manjimup" ,"coordinates": [115.3365,-35.0689,116.8568,-33.4404]}
{ "name":"Esperance" ,"coordinates": [119.1841,-34.4747,124.2293,-32.5647]}
{ "name":"Gascoyne" ,"coordinates": [112.9211,-27.2979,117.8710,-21.7858]}
{ "name":"Goldfields" ,"coordinates": [118.4359,-33.0281,129.0019,-23.4319]}
{ "name":"Kimberley" ,"coordinates": [120.0641,-21.2320,129.0013,-13.6895]}
{ "name":"Mid West" ,"coordinates": [113.5883,-30.2086,124.6680,-23.2933]}
{ "name":"Pilbara" ,"coordinates": [114.3040,-23.8879,129.0013,-19.4986]}
{ "name":"Albany" ,"coordinates": [116.6690,-35.1348,119.5412,-33.2727]}
{ "name":"Wheat" ,"coordinates": [114.9704,-32.3578,120.0139,-29.6123]}
{ "name":"Melbourne" ,"coordinates": [144.9514,-37.8231,144.9749,-37.8059]}
{ "name":"Melb" ,"coordinates": [144.9514,-37.8231,144.9749,-37.8059]}
{ "name":"Sydney" ,"coordinates": [151.1715,-33.9243,151.2318,-33.8509]}
{ "name":"Syd" ,"coordinates": [151.1715,-33.9243,151.2318,-33.8509]}
{ "name":"Darwin City" ,"coordinates": [130.8151,-12.4737,130.9086,-12.3953]}
{ "name":"Brisbane" ,"coordinates": [152.9583,-27.4938,153.0896,-27.4040]}
{ "name":"Adelaide" ,"coordinates": [138.4362,-35.3503,139.0440,-34.5002]}
{ "name":"Perth" ,"coordinates": [115.4495,-32.8019,116.4151,-31.4551]}
{ "name":"Western Australia" ,"coordinates": [115.4495,-32.8019,116.4151,-31.4551]}
{ "name":"Tasmania" ,"coordinates": [147.0267,-43.1213,147.9369,-42.6554]}
{ "name":"South Australia" ,"coordinates": [138.4362,-35.3503,139.0440,-34.5002]}
{ "name":"QL" ,"coordinates": [152.9583,-27.4938,153.0896,-27.4040]}
{ "name":"QLD" ,"coordinates": [152.9583,-27.4938,153.0896,-27.4040]}
{ "name":"Queensland" ,"coordinates": [152.9583,-27.4938,153.0896,-27.4040]}
{ "name":"NSW " ,"coordinates": [151.1715,-33.9243,151.2318,-33.8509]}
{ "name":"New South Wales " ,"coordinates": [151.1715,-33.9243,151.2318,-33.8509]}
{ "name":"Victoria" ,"coordinates": [144.9514,-37.8231,144.9749,-37.8059]}
{ "name":"Vic" ,"coordinates": [144.9514,-37.8231,144.9749,-37.8059]}