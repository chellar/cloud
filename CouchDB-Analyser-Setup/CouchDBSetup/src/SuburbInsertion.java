/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Program used to initialise the database for the purpose of analysis
*/

import java.io.File;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

public class SuburbInsertion {

	// extracts each suburb from the source file (Suburb.txt) and stores them in the database
	public static void main(String[] args) {
		// connection to the database
		CouchDbClient dbClientSuburb = new CouchDbClient("suburb-team", true, "http", "115.146.93.181", 5984, "admin", "123456");
		Response response;
		try {
			JSONParser parser = new JSONParser();
			File file = new File("Suburbs.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while(inputKeywords.hasNextLine()) {
				String details = inputKeywords.nextLine();
				JSONObject TwitterObject = (JSONObject) parser.parse(details);
				String name = (String) TwitterObject.get("name");
				JSONArray coordinates = (JSONArray) TwitterObject.get("coordinates");
				Double maxLongitude = (Double) coordinates.get(2);
				Double minLongitude = (Double) coordinates.get(0);
				Double maxLatitude = (Double) coordinates.get(3);
				Double minLatitude = (Double) coordinates.get(1);
				Suburb suburb = new Suburb(name, maxLongitude,
						minLongitude, maxLatitude, minLatitude);
				response = dbClientSuburb.save(suburb);
			}
			inputKeywords.close();		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
