/**
* Team 17: Vivek Kumar, Aman Lal, Chella Ramachandran, Sanaan Ahmed, Muhammad Taimoor Khan 
* Class used to extract old twitter data and store in the database
*/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.lightcouch.CouchDbClient;
import org.lightcouch.Response;

import twitter4j.FilterQuery;
import twitter4j.IDs;
import twitter4j.PagableResponseList;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.Query.ResultType;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

public class TwitterExtractor {

	/** Information necessary for accessing the Twitter API */
	private String consumerKey;
	private String consumerSecret;
	private String accessToken;
	private String accessTokenSecret;
	private ArrayList<String> keywords;
	private ArrayList<Grid> grids;

	/** The actual Twitter stream. It's set up to collect raw JSON data */
	private Twitter twitter;
	
	public TwitterExtractor() {
		keywords = new ArrayList<String>();
		grids = new ArrayList<Grid>();
	}

	// configures the keys twitter authentication
	public void configure() {
		consumerKey = "bkAoj3VqriOT4VzoJD0HGZBiC";
		consumerSecret = "tchBgUBqE9pj9KthbNBfo5nNOhINKEP0Fej9sSTjpBgwKjcQXW";
		accessToken = "858162138220683265-zTEfKU2fZt6AqY3Mjs8XyqgPw50lgtl";
		accessTokenSecret = "pBICpJvIwoCIc1qHb2npJdl532Eszq37A9u6k9lvjgFyT";

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey(consumerKey);
		cb.setOAuthConsumerSecret(consumerSecret);
		cb.setOAuthAccessToken(accessToken);
		cb.setOAuthAccessTokenSecret(accessTokenSecret);
		cb.setJSONStoreEnabled(true);
		cb.setIncludeEntitiesEnabled(true);
		twitter = new TwitterFactory(cb.build()).getInstance();
	}

	// start of execution
	public static void main(String[] args) {		
		TwitterExtractor twitterExtractor = new TwitterExtractor();
		twitterExtractor.configure();
		twitterExtractor.extractSuburbs();
		twitterExtractor.setKeywords();
		try {
			System.setOut(new PrintStream(new File("output.txt")));
			twitterExtractor.searchQuery();
		} catch (TwitterException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	// searches for the tweets based on the query
	public void searchQuery() throws TwitterException, ParseException {
		// database connention
		CouchDbClient dbClient = new CouchDbClient("tweets", true, "http", "115.146.93.181", 5984, "admin", "123456");
		JSONParser parser = new JSONParser();
		Response response;
		Iterator<String> keyWordIterator = keywords.iterator();
		while (keyWordIterator.hasNext()) {
			String keyword = keyWordIterator.next();
			Query query = new Query(keyword);
			query.resultType(ResultType.mixed);
			query.setLang("en");
	        query.setCount(200);
	        query.since("2017-04-01");
	        query.until("2017-05-07");
			QueryResult result;
	        result = twitter.search(query);
	        checkRateLimit(result);
			ArrayList<Status> tweets = new ArrayList<Status>();
			do {
				List<Status> newTweets = result.getTweets();
				if (tweets == null) {
					tweets = (ArrayList<Status>) newTweets;
				} else {
					tweets.addAll(newTweets);
				}
				Iterator<Status> iter = newTweets.iterator();
				while (iter.hasNext()) {
					Status status = iter.next();
					String json = "";
					// if geolocation is not null and is australian or
					// if timezone, place or userlocation is not null
					// then extract the json
					if (status.getGeoLocation() != null) {
						// Australian coordinates
						Double maxLatitude = -9.1422;
						Double maxLongitude = 159.1092;
						Double minLatitude = -43.7405;
						Double minLongitude = 96.8168;
						Double latitude = status.getGeoLocation().getLatitude();
						Double longitude = status.getGeoLocation().getLongitude();
						if ((latitude >= minLatitude) && (latitude <= maxLatitude) && (longitude >= minLongitude)
								&& (longitude <= maxLongitude)) {
							json = DataObjectFactory.getRawJSON(status);
						}
					} else if ((status.getPlace() != null) || (status.getUser().getLocation() != null)
							|| (status.getUser().getTimeZone() != null)) {
						json = DataObjectFactory.getRawJSON(status);
					}
					if(!(json.equals(""))) {
						String country = "";
						String city = "";
						double longitude = 0;
						double latitude = 0;
						JSONObject TwitterObject = new JSONObject();
						TwitterObject = (JSONObject) parser.parse(json);
						Long id = (Long) TwitterObject.get("id");
						String text = (String) TwitterObject.get("text");
						text = text.replace("\n", " ");
						JSONObject place = (JSONObject) TwitterObject.get("place");
						if (place != null) {
							country = (String) place.get("country");
							city = (String) place.get("name");
						}
						JSONObject coordinates = (JSONObject) TwitterObject.get("coordinates");
						if (coordinates != null) {
							JSONArray coordinatesArray = (JSONArray) parser
									.parse(coordinates.get("coordinates").toString());
							if (coordinatesArray != null) {
								longitude = Double.parseDouble(coordinatesArray.get(0).toString());
								latitude = Double.parseDouble(coordinatesArray.get(1).toString());
							}
						} 
						JSONObject user = (JSONObject) TwitterObject.get("user");
						String screenName = (String) user.get("screen_name");
						String userLocation = "";
						if(user.get("location") != null) {
							userLocation = (String) user.get("location");
						}
						String userTimeZone = "";
						if(user.get("time_zone") != null) {
							userTimeZone = (String) user.get("time_zone");
						}
						Tweet tweet = new Tweet(text, country, city, screenName, userLocation,
								userTimeZone);
						tweet.setId(id.toString());
						boolean locationFound = false;
						// if geo-coordinates are present find the associated suburb
						if (longitude != 0.0 && latitude != 0.0) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								boolean isInside = grid.ifInside(longitude, latitude);
								if(isInside) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						// if city is not null, check if it is a suburb in australia
						if ((tweet.getCity() != null) && !(tweet.getCity().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getCity().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						// if userlocation is not null, check if it a suburb in australia
						if ((tweet.getUserLocation() != null) && !(tweet.getUserLocation().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getUserLocation().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						// if timezone is not null, then check whether the suburb is in australia
						if ((tweet.getUserTimeZone() != null) && !(tweet.getUserTimeZone().equals("")) && !locationFound) {
							Iterator<Grid> iterator = grids.iterator();
							while(iterator.hasNext()) {
								Grid grid = iterator.next();
								if(tweet.getUserTimeZone().contains(grid.getAreaName())) {
									tweet.setSuburb(grid.getAreaName());
									tweet.setCordinates(grid);
									locationFound = true;
									break;
								}
							}
						}
						// if location is found save the tweet, else discard
						if(locationFound) {
							if(!(tweet.getUserTimeZone().contains("usa") || tweet.getUserTimeZone().contains("london")
									|| tweet.getCity().contains("usa") || tweet.getCity().contains("london")
									|| tweet.getUserLocation().contains("usa") || tweet.getUserLocation().contains("london"))) {
								 response = dbClient.save(tweet);
							}
						}
					}
				}
				tweets = new ArrayList<Status>();
				query = result.nextQuery();
				if(query != null) {
					result = twitter.search(query);
					checkRateLimit(result);
				}
			} while (result == null || result.hasNext());
		}
	}
	
	// checks whether the limit is exceeded and if true waits till reset
	public void checkRateLimit() {
		try {
			Map<String ,RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus();
			for (String endpoint : rateLimitStatus.keySet()) {
			    RateLimitStatus status = rateLimitStatus.get(endpoint);
			    if(status.getRemaining() <= 0) {
			    	Thread.sleep(status.getSecondsUntilReset() * 1000);
			    }
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void checkRateLimit(QueryResult result) {
	    if (result.getRateLimitStatus().getRemaining() <= 0){
	        try {
	        	Thread.sleep(result.getRateLimitStatus().getSecondsUntilReset() * 1000);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	            throw new RuntimeException(e);
	        }
	    }
	}
	
	// extracts and stores the suburbs in to a list
	public void extractSuburbs() {
		try {
			JSONParser parser = new JSONParser();
			File file = new File("Suburbs.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while(inputKeywords.hasNextLine()) {
				String suburb = inputKeywords.nextLine();
				JSONObject TwitterObject = (JSONObject) parser.parse(suburb);
				String name = (String) TwitterObject.get("name");
				JSONArray coordinates = (JSONArray) TwitterObject.get("coordinates");
				Double maxLongitude = (Double) coordinates.get(2);
				Double minLongitude = (Double) coordinates.get(0);
				Double maxLatitude = (Double) coordinates.get(3);
				Double minLatitude = (Double) coordinates.get(1);
				Grid grid = new Grid(name, maxLongitude, minLongitude, maxLatitude,
						minLatitude);
				grids.add(grid);
			}
			inputKeywords.close();		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	// extracts and stores the keywords into a list
	public void setKeywords() {
		try {
			File file = new File("Keywords.txt");
			Scanner inputKeywords = new Scanner(System.in);
			inputKeywords = new Scanner(file);
			while(inputKeywords.hasNextLine()) {
				String keyword = inputKeywords.nextLine();
				keywords.add(keyword);
			}
			inputKeywords.close();		
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}